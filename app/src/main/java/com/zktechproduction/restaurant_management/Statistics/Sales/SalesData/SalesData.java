package com.zktechproduction.restaurant_management.Statistics.Sales.SalesData;

/**
 * Created by ZelcaKok on 25/3/2018.
 */

public class SalesData {
    private CHART_TYPE type;

    public SalesData(CHART_TYPE type) {
        this.type = type;
    }

    public CHART_TYPE getType() {
        return type;
    }

    public void setType(CHART_TYPE type) {
        this.type = type;
    }

    public enum CHART_TYPE {
        LINE, BAR, PIE
    }
}
