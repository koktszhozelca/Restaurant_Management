package com.zktechproduction.restaurant_management.Statistics.Sales.SalesData;

/**
 * Created by ZelcaKok on 25/3/2018.
 */

public class Revenue extends SalesData {

    private long timestamp;
    private float value;

    public Revenue(CHART_TYPE type, long timestamp, float value) {
        super(type);
        this.timestamp = timestamp;
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
