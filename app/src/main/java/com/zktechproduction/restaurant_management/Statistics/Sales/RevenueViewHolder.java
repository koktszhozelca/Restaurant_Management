package com.zktechproduction.restaurant_management.Statistics.Sales;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.zktechproduction.restaurant_management.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ZelcaKok on 25/3/2018.
 */

public class RevenueViewHolder extends RecyclerView.ViewHolder {
    static String TAG = "[RevenueViewHolder]";

    public TextView mTextView;
    public LineChart chart;
    public TextView btnShare;
    public MaterialSpinner spinner;

    public RevenueViewHolder(final View v) {
        super(v);
        mTextView = v.findViewById(R.id.title);
        chart = v.findViewById(R.id.chart);
        btnShare = v.findViewById(R.id.btnShare);
        spinner = v.findViewById(R.id.selFilter);

        //TODO Setup the filters
        final ArrayList<String> filters = new ArrayList<String>() {{
            add("Day");
            add("Month");
            add("Week");
            add("Year");
        }};
        spinner.setItems(filters);
        spinner.setSelectedIndex(0);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Toast.makeText(v.getContext(), filters.get(position) + " is selected.", Toast.LENGTH_LONG).show();
            }
        });

        HashMap<Integer, String> labels = new HashMap<Integer, String>() {{
            put(0, "13:00 1 Mar 2018");
            put(1, "15:00 1 Mar 2018");
            put(2, "18:00 1 Mar 2018");
        }};

        chartConfig(labels, chart);
        setData(chart);
    }

    private void chartConfig(final HashMap<Integer, String> labels, LineChart chart) {
        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Log.d(TAG, "VALUE: " + String.valueOf(value) + labels.get((int) value));
                return labels.get((int) value);
            }
        });
        xAxis.setTextSize(11f);
        Legend l = chart.getLegend();
        l.setTextSize(11f);
    }

    private void setData(LineChart chart) {
        List<Entry> entries = new ArrayList<Entry>();

        entries.add(new Entry(0, 0));
        entries.add(new Entry(1, 1));
        entries.add(new Entry(2, 2));

        LineDataSet dataSet = new LineDataSet(entries, "Sales");
        dataSet.setLineWidth(5f);
        dataSet.setValueTextSize(11f);

        LineData data = new LineData(dataSet);

        chart.setData(data);
        chart.invalidate();
    }
}