package com.zktechproduction.restaurant_management.Statistics.Sales;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.zktechproduction.restaurant_management.R;

import java.util.ArrayList;

/**
 * Created by ZelcaKok on 25/3/2018.
 */

public class DishesViewHolder extends RecyclerView.ViewHolder {
    public TextView mTextView;
    public BarChart chart;
    public TextView btnShare;
    public MaterialSpinner spinner;

    public DishesViewHolder(final View v) {
        super(v);
        mTextView = v.findViewById(R.id.title);
        chart = v.findViewById(R.id.chart);
        btnShare = v.findViewById(R.id.btnShare);
        spinner = v.findViewById(R.id.selFilter);

        //TODO Setup the filters
        final ArrayList<String> filters = new ArrayList<String>() {{
            add("Day");
            add("Month");
            add("Week");
            add("Year");
        }};
        spinner.setItems(filters);
        spinner.setSelectedIndex(0);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Toast.makeText(v.getContext(), filters.get(position) + " is selected.", Toast.LENGTH_LONG).show();
            }
        });

        //TODO Data setup
        chartConfig(chart);
        setData(chart);
    }

    private void setData(BarChart chart) {
        ArrayList<BarEntry> BarEntry = new ArrayList<>();
        BarEntry.add(new BarEntry(0, 30, "Carbonra"));
        BarEntry.add(new BarEntry(1, 45, "Risotto"));

        BarDataSet dataSet = new BarDataSet(BarEntry, "Dishes");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData data = new BarData(dataSet);
        data.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return entry.getData().toString();
            }
        });

        data.setValueTextSize(11f);
        chart.setData(data);
        chart.invalidate();
    }

    private void chartConfig(BarChart chart) {
        Description desc = new Description();
        desc.setText("Dishes sales information");
        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setEnabled(false);
        xAxis.setGranularityEnabled(false);
        chart.setDescription(desc);
    }


}