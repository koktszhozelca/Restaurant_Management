package com.zktechproduction.restaurant_management.Statistics.Sales;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zktechproduction.restaurant_management.R;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class SalesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static String TAG = "[SalesAdapter]";
    private String[] mDataSet;

    public SalesAdapter(String[] mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        Log.d(TAG, "ViewType: " + String.valueOf(viewType));
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sales_chart_layout, parent, false);
                return new RevenueViewHolder(v);
            case 1:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dishes_chart_layout, parent, false);
                return new DishesViewHolder(v);
            case 2:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.goods_chart_layout, parent, false);
                return new GoodsViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(TAG, "Position: " + String.valueOf(position));
        switch (position) {
            case 0:
                RevenueViewHolder revenueViewHolder = (RevenueViewHolder) holder;
                revenueViewHolder.mTextView.setText(mDataSet[position]);
                break;
            case 1:
                DishesViewHolder dishesViewHolder = (DishesViewHolder) holder;
                dishesViewHolder.mTextView.setText(mDataSet[position]);
                break;
            case 2:
                GoodsViewHolder goodsViewHolder = (GoodsViewHolder) holder;
                goodsViewHolder.mTextView.setText(mDataSet[position]);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }
}
