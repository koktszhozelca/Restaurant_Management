package com.zktechproduction.restaurant_management.Statistics.Sales;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zktechproduction.restaurant_management.R;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class SalesFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sales_layout, container, false);

        //Setup the recycler view here
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //TODO Prepare data for debug
        String[] dataSet = {"Sales data by day, week, month", "Dishes sales data", "Goods consumption"};
//        String[] dataSet = {"Sales data by day, week, month", "Dishes sales data"};
        mAdapter = new SalesAdapter(dataSet);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }
}
