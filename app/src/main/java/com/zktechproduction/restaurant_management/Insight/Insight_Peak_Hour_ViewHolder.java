package com.zktechproduction.restaurant_management.Insight;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.zktechproduction.restaurant_management.R;

import java.util.ArrayList;

import static com.github.mikephil.charting.utils.ColorTemplate.rgb;

/**
 * Created by ZelcaKok on 26/3/2018.
 */

public class Insight_Peak_Hour_ViewHolder extends RecyclerView.ViewHolder {
    public TextView mTextView;
    private PieChart chart;
    private Activity activity;

    public Insight_Peak_Hour_ViewHolder(View v, Activity activity) {
        super(v);
        mTextView = v.findViewById(R.id.title);
        chart = v.findViewById(R.id.chart);
        this.activity = activity;

        chartConfig(chart);
        setData(chart, 4, 10);
    }

    private void chartConfig(PieChart chart) {
        chart.setBackgroundColor(Color.WHITE);
        moveOffScreen(this.activity, chart);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationEnabled(false);
        chart.setHighlightPerTapEnabled(true);

        chart.setMaxAngle(180f); // HALF CHART
        chart.setRotationAngle(180f);
        chart.setCenterTextOffset(0, -20);

        chart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTextSize(12f);
    }

    private void setData(PieChart chart, int count, float range) {
        int[] colorSchema = {rgb("#2ecc71"), rgb("#f1c40f"), rgb("#3498db"), rgb("#e74c3c")};

        String[] mParties = {"08:00 - 11:29", "11:30 - 14:00", "14:01 - 18:30", "18:31 - 23:00"};
        ArrayList<PieEntry> values = new ArrayList<PieEntry>();

        values.add(new PieEntry(20, mParties[0]));
        values.add(new PieEntry(70, mParties[1]));
        values.add(new PieEntry(19, mParties[2]));
        values.add(new PieEntry(80, mParties[3]));

        PieDataSet dataSet = new PieDataSet(values, "Order Count");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        dataSet.setColors(colorSchema);

//        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        chart.setData(data);

        chart.invalidate();
    }

    private void moveOffScreen(Activity activity, PieChart chart) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        int offset = (int) (height * 0.2); /* percent to move */

        RelativeLayout.LayoutParams rlParams =
                (RelativeLayout.LayoutParams) chart.getLayoutParams();
        rlParams.setMargins(0, 0, 0, -offset);
        chart.setLayoutParams(rlParams);
    }
}