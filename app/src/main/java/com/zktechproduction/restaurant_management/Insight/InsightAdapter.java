package com.zktechproduction.restaurant_management.Insight;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zktechproduction.restaurant_management.R;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class InsightAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private String[] mDataSet;
    private Activity activity;

    public InsightAdapter(Activity activity, String[] mDataSet) {
        this.mDataSet = mDataSet;
        this.activity = activity;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.insight_chart_layout, parent, false);
                return new Insight_Peak_Hour_ViewHolder(v, this.activity);
            case 1:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.insight_chart_layout, parent, false);
                return new Insight_Peak_Hour_ViewHolder(v, this.activity);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0)
            ((Insight_Peak_Hour_ViewHolder) holder).mTextView.setText(mDataSet[position]);
//        else if(position==1) ((Insight_Dish_Assoc_ViewHolder) holder).mTextView.setText(mDataSet[position]);
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }


}
