package com.zktechproduction.restaurant_management.Components.Confirmation;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.zktechproduction.restaurant_management.R;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class ConfirmationDialog extends DialogFragment {

    public static Dialog confirm(Activity activity, String msg, final ConfirmationHandler handler) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        handler.positive();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        handler.negative();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
