package com.zktechproduction.restaurant_management.Components.SelectGoods;

import android.app.Dialog;
import android.content.Context;

import com.zktechproduction.restaurant_management.R;

/**
 * Created by ZelcaKok on 25/3/2018.
 */

public class SelectGoodsDialog {
    public SelectGoodsDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.select_goods_layout);
        dialog.setTitle("Select Ingredients");
        dialog.show();
    }
}
