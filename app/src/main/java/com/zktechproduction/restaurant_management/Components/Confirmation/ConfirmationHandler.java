package com.zktechproduction.restaurant_management.Components.Confirmation;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public interface ConfirmationHandler {
    public void positive();

    public void negative();
}
