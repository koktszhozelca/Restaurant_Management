package com.zktechproduction.restaurant_management.Configuration;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class Config {
    public enum MENU_LIST {
        SALE, MAN_GOODS, MAN_DISHES, INSIGHT, CREATE_DISH, CREATE_GOODS
    }
}
