package com.zktechproduction.restaurant_management.Management.Dishes;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zktechproduction.restaurant_management.Components.Camera.CameraActivity;
import com.zktechproduction.restaurant_management.Components.SelectGoods.SelectGoodsDialog;
import com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB.DBClient;
import com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB.OnDataWroteListener;
import com.zktechproduction.restaurant_management.FirebaseClient.Storage.OnDownloadUriGenerateListener;
import com.zktechproduction.restaurant_management.FirebaseClient.Storage.StorageClient;
import com.zktechproduction.restaurant_management.R;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ZelcaKok on 25/3/2018.
 */

public class CreateDishFragment extends Fragment {
    private static final int CHOOSE_PIC = 300;
    static String TAG = "[CreateDish]";
    private ImageView featuredImg;
    private EditText txtName;
    private TextView btnCreate, txtIngredients;
    private Uri imgUri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_dish_layout, container, false);

        final List<String> availGoods = new ArrayList<>();

        featuredImg = view.findViewById(R.id.img);
        txtName = view.findViewById(R.id.txtGoodsName);
        btnCreate = view.findViewById(R.id.btnCreate);
        txtIngredients = view.findViewById(R.id.txtIngredients);


        featuredImg.setOnClickListener(new SelectImage());
        txtIngredients.setOnClickListener(new SelectGoods());

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name = txtName.getText().toString();
                //TODO Upload the image and get back the download uri
                StorageClient storageClient = StorageClient.getInstance();
                try {
                    storageClient.upload("image/" + name, getActivity().getContentResolver().openInputStream(imgUri), new OnDownloadUriGenerateListener() {
                        @Override
                        public void onGenerated(Uri uri) {
                            //TODO Create a new dish
                            DBClient dbC = DBClient.getInstance();
                            dbC.append("/Dishes", new Dish(name, uri.toString(), null), new OnDataWroteListener() {
                                @Override
                                public void finish() {
                                    Toast.makeText(getActivity(), "Dish is created", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                //TODO Select goods from control
//                Toast.makeText(getActivity(), "Create new dish", Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }

    private Drawable getImageFromUri(Uri uri) throws FileNotFoundException {
        InputStream inputStream = null;
        inputStream = getActivity().getContentResolver().openInputStream(uri);
        return Drawable.createFromStream(inputStream, uri.toString());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CHOOSE_PIC) {
            try {
                Bundle bundle = intent.getExtras();
                imgUri = Uri.parse(bundle.getString("imgUri"));
                Drawable img = getImageFromUri(imgUri);
                featuredImg.setImageDrawable(img);
            } catch (Exception e) {

            }
        }
    }

    private class SelectImage implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), CameraActivity.class);
            startActivityForResult(intent, CHOOSE_PIC);
        }
    }

    private class SelectGoods implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            new SelectGoodsDialog(getActivity());
        }
    }
}
