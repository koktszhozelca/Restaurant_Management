package com.zktechproduction.restaurant_management.Management.Dishes;

import android.graphics.drawable.Drawable;

import com.zktechproduction.restaurant_management.Management.Goods.Goods;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class Dish {
    private String name, imgUrl;
    private Goods[] ingredients;

    public Dish() {
    }

    public Dish(String name, String imgUrl, Goods[] ingredients) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.ingredients = ingredients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Goods[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(Goods[] ingredients) {
        this.ingredients = ingredients;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
