package com.zktechproduction.restaurant_management.Management.Goods;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zktechproduction.restaurant_management.R;

import java.util.ArrayList;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class GoodsAdapter extends RecyclerView.Adapter<GoodsAdapter.ViewHolder> {
    static String TAG = "[GoodsAdapter]";
    private ArrayList<Goods> mDataSet;

    public GoodsAdapter(ArrayList<Goods> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.goods_card_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Goods goods = mDataSet.get(position);
        holder.mTextView.setText(goods.getName());
        holder.txtRemain.setText("Remain " + String.valueOf(goods.getQuantity()));
        Picasso.get()
                .load(mDataSet.get(position).getImgUrl())
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
                .into(holder.featuredImage);
        holder.mTextView.setOnClickListener(new CardAction(goods));
        holder.txtRemain.setOnClickListener(new CardAction(goods));
        holder.featuredImage.setOnClickListener(new CardAction(goods));
        holder.btnPurchase.setOnClickListener(new CardAction(goods));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView, txtRemain, btnPurchase;
        public ImageView featuredImage;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.title);
            txtRemain = v.findViewById(R.id.txtRemain);
            featuredImage = v.findViewById(R.id.featuredImage);
            btnPurchase = v.findViewById(R.id.btnPurchase);
        }
    }

    private class CardAction implements View.OnClickListener {
        private Goods goods;

        public CardAction(Goods goods) {
            this.goods = goods;
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "Show Info: " + this.goods.getName());
        }
    }


}
