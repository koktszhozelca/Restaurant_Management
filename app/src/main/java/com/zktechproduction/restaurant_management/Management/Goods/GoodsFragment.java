package com.zktechproduction.restaurant_management.Management.Goods;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zktechproduction.restaurant_management.Configuration.Config;
import com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB.DBClient;
import com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB.OnDataReceiveListener;
import com.zktechproduction.restaurant_management.MainActivity;
import com.zktechproduction.restaurant_management.R;

import java.util.ArrayList;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class GoodsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private FloatingActionButton fab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.goods_layout, container, false);
        //Setup the recycler view here
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        DBClient dbC = DBClient.getInstance();
        final ArrayList<Goods> dataSet = new ArrayList<>();
        dbC.monitor("/Goods", Goods.class, new OnDataReceiveListener() {
            @Override
            public void receive(ArrayList<Object> datas) {
                dataSet.clear();
                for (Object obj : datas) dataSet.add((Goods) obj);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.invalidate();
            }
        });
        mAdapter = new GoodsAdapter(dataSet);
        mRecyclerView.setAdapter(mAdapter);

        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.changeFragment((AppCompatActivity) getActivity(), Config.MENU_LIST.CREATE_GOODS);
            }
        });

        return view;
    }
}
