package com.zktechproduction.restaurant_management.Management.Goods;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class Goods {
    private String name, imgUrl, unit, status;
    private int quantity;

    public Goods() {
    }

    public Goods(String name, String imgUrl, String unit, String status, int quantity) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.unit = unit;
        this.status = status;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
