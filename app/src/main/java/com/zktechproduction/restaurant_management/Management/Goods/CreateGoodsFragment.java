package com.zktechproduction.restaurant_management.Management.Goods;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.zktechproduction.restaurant_management.Components.Camera.CameraActivity;
import com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB.DBClient;
import com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB.OnDataWroteListener;
import com.zktechproduction.restaurant_management.FirebaseClient.Storage.OnDownloadUriGenerateListener;
import com.zktechproduction.restaurant_management.FirebaseClient.Storage.StorageClient;
import com.zktechproduction.restaurant_management.R;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class CreateGoodsFragment extends Fragment {
    private final int CHOOSE_PIC = 100;

    private TextView btnCreate;
    private EditText txtName, txtNumber;
    private ImageView featuredImg;
    private MaterialSpinner status, unit;
    private Uri imgUri;

    private String[] statusTag = {"Order", "Remain"};
    private String[] unitTag = {"pack", "kg", "g", "L"};

    private Drawable getImageFromUri(Uri uri) throws FileNotFoundException {
        InputStream inputStream = null;
        inputStream = getActivity().getContentResolver().openInputStream(uri);
        return Drawable.createFromStream(inputStream, uri.toString());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_goods_layout, container, false);
        txtName = view.findViewById(R.id.txtGoodsName);
        txtNumber = view.findViewById(R.id.txtNumber);
        btnCreate = view.findViewById(R.id.btnCreate);
        featuredImg = view.findViewById(R.id.img);
        status = view.findViewById(R.id.status);
        unit = view.findViewById(R.id.unit);


        featuredImg.setOnClickListener(new SelectImage());

        status.setItems(statusTag);
        unit.setItems(unitTag);


        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Gather all the information
                final String goodsName = txtName.getText().toString();
                final int quantity = Integer.parseInt(txtNumber.getText().toString());
                final String goodsStatus = statusTag[status.getSelectedIndex()];
                final String goodsUnit = unitTag[unit.getSelectedIndex()];

                //TODO Upload the image to storage
                StorageClient storageClient = StorageClient.getInstance();
                try {
                    storageClient.upload("image/" + goodsName, getActivity().getContentResolver().openInputStream(imgUri), new OnDownloadUriGenerateListener() {
                        @Override
                        public void onGenerated(Uri uri) {
                            //TODO Create a new goods
                            Goods goods = new Goods(goodsName, uri.toString(), goodsUnit, goodsStatus, quantity);
                            DBClient dbC = DBClient.getInstance();
                            dbC.append("/Goods", goods, new OnDataWroteListener() {
                                @Override
                                public void finish() {
                                    Toast.makeText(getActivity(), "Goods is created", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CHOOSE_PIC) {
            try {
                Bundle bundle = intent.getExtras();
                imgUri = Uri.parse(bundle.getString("imgUri"));
                Drawable img = getImageFromUri(imgUri);
                featuredImg.setImageDrawable(img);
            } catch (Exception e) {

            }
        }
    }

    private class SelectImage implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), CameraActivity.class);
            startActivityForResult(intent, CHOOSE_PIC);
        }
    }
}

