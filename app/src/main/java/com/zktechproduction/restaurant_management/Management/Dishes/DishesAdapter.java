package com.zktechproduction.restaurant_management.Management.Dishes;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zktechproduction.restaurant_management.R;

import java.util.ArrayList;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class DishesAdapter extends RecyclerView.Adapter<DishesAdapter.ViewHolder> {

    static String TAG = "[DishAdapter]";
    private ArrayList<Dish> mDataSet;

    public DishesAdapter(ArrayList<Dish> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_card_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mTextView.setText(mDataSet.get(position).getName());
        Picasso.get()
                .load(mDataSet.get(position).getImgUrl())
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
                .into(holder.featuredImage);
        holder.btnShowInfo.setOnClickListener(new CardAction(mDataSet.get(position)));
        holder.cardView.setOnClickListener(new CardAction(mDataSet.get(position)));
        holder.featuredImage.setOnClickListener(new CardAction(mDataSet.get(position)));
        holder.mTextView.setOnClickListener(new CardAction(mDataSet.get(position)));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView, btnShowInfo;
        public ImageView featuredImage;
        public CardView cardView;

        public ViewHolder(View v) {
            super(v);
            cardView = v.findViewById(R.id.card_view);
            mTextView = v.findViewById(R.id.title);
            featuredImage = v.findViewById(R.id.featuredImage);
            btnShowInfo = v.findViewById(R.id.btnShowInfo);
        }
    }

    private class CardAction implements View.OnClickListener {
        private Dish dish;

        public CardAction(Dish dish) {
            this.dish = dish;
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "Show Info: " + this.dish.getName());
        }
    }


}
