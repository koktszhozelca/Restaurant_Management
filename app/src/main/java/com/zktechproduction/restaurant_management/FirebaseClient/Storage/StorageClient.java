package com.zktechproduction.restaurant_management.FirebaseClient.Storage;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ZelcaKok on 26/3/2018.
 */

public class StorageClient {
    private static final StorageClient ourInstance = new StorageClient();
    static String TAG = "[StorageClient]";
    private FirebaseStorage storage;

    private StorageClient() {
        storage = FirebaseStorage.getInstance("gs://restaurant-comp4342.appspot.com/");
    }

    public static StorageClient getInstance() {
        return ourInstance;
    }

    public void upload(String ref, InputStream stream, final OnDownloadUriGenerateListener listener) {
        storage.getReference(ref).putStream(stream)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        listener.onGenerated(taskSnapshot.getDownloadUrl());
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Storage Client Error: " + e.getMessage());
            }
        });
    }

    public void download(String ref, final OnFileReceiveListener listener) throws IOException {
        final File localFile = File.createTempFile("image", "jpg");
        StorageReference riversRef = storage.getReference().child(ref);
        riversRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Successfully downloaded data to local file
                        // ...
                        Uri fileUri = Uri.fromFile(localFile);
                        listener.onReceive(fileUri);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
                Log.d(TAG, "Downloaded failure");
            }
        });
    }
}
