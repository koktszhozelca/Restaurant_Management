package com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public interface OnDataSnapshotReceiveListener {
    public void receive(DataSnapshot dataSnapshot);
}
