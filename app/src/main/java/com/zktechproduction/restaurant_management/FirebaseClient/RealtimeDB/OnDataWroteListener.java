package com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public interface OnDataWroteListener {
    public void finish();
}
