package com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ZelcaKok on 24/3/2018.
 */
@IgnoreExtraProperties
public class D {
    private String name;
    private int age;

    public D() {
    }

    public D(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "D{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
