package com.zktechproduction.restaurant_management.FirebaseClient.Storage;

import android.net.Uri;

/**
 * Created by ZelcaKok on 26/3/2018.
 */

public interface OnDownloadUriGenerateListener {
    public void onGenerated(Uri uri);
}
