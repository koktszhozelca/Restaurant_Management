package com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB;

import java.util.ArrayList;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public interface OnDataReceiveListener {
    public void receive(ArrayList<Object> datas);
}
