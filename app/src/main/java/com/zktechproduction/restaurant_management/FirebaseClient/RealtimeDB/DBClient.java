package com.zktechproduction.restaurant_management.FirebaseClient.RealtimeDB;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by ZelcaKok on 24/3/2018.
 */

public class DBClient {
    private static final DBClient instance = new DBClient();
    private static String TAG = "[DBClient]";
    private FirebaseDatabase database;

    private DBClient() {
        this.database = FirebaseDatabase.getInstance();
    }

    public static DBClient getInstance() {
        return instance;
    }

    public void write(String ref, Object obj, @Nullable OnDataWroteListener listener) {
        this.database.getReference(ref).setValue(obj);
        if (listener != null) listener.finish();
    }

    public void append(String ref, Object obj, @Nullable OnDataWroteListener listener) {
        this.database.getReference(ref).push().setValue(obj);
        if (listener != null) listener.finish();
    }

    public void read(String ref, final Class targetClass, final OnDataReceiveListener listener) {
        this.database.getReference(ref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Object> datas = new ArrayList<>();
                if (dataSnapshot.hasChildren())
                    for (DataSnapshot data : dataSnapshot.getChildren())
                        datas.add(data.getValue(targetClass));
                else datas.add(dataSnapshot.getValue(targetClass));
                listener.receive(datas);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "DB Error" + databaseError.getMessage());
            }
        });
    }

    public void read(String ref, final OnDataSnapshotReceiveListener listener) {
        this.database.getReference(ref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listener.receive(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "DB Error" + databaseError.getMessage());
            }
        });
    }

    public void monitor(String ref, final Class targetClass, final OnDataReceiveListener listener) {
        this.database.getReference(ref).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Object> datas = new ArrayList<>();
                if (dataSnapshot.hasChildren())
                    for (DataSnapshot data : dataSnapshot.getChildren())
                        datas.add(data.getValue(targetClass));
                else datas.add(dataSnapshot.getValue(targetClass));
                listener.receive(datas);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "DB Error" + databaseError.getMessage());
            }
        });
    }

    public void wipe(String ref, @Nullable OnWipeFinishListener listener) {
        this.database.getReference(ref).setValue(null);
        if (listener != null) listener.finish();
    }
}
