package com.zktechproduction.restaurant_management;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.zktechproduction.restaurant_management.Components.Confirmation.ConfirmationDialog;
import com.zktechproduction.restaurant_management.Components.Confirmation.ConfirmationHandler;
import com.zktechproduction.restaurant_management.Configuration.Config.MENU_LIST;
import com.zktechproduction.restaurant_management.Insight.InsightFragment;
import com.zktechproduction.restaurant_management.Management.Dishes.CreateDishFragment;
import com.zktechproduction.restaurant_management.Management.Dishes.DishesFragment;
import com.zktechproduction.restaurant_management.Management.Goods.CreateGoodsFragment;
import com.zktechproduction.restaurant_management.Management.Goods.GoodsFragment;
import com.zktechproduction.restaurant_management.Statistics.Sales.SalesFragment;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static FragmentManager fm;
    public static FragmentTransaction ft;
    static String TAG = "[Main]";
    static Map<MENU_LIST, Fragment> fragments;

    TextView userEmail, displayName;

    private static Fragment prepareFragments(MENU_LIST id) {
        if (!fragments.containsKey(id))
            switch (id) {
                case SALE:
                    fragments.put(id, new SalesFragment());
                    break;
                case MAN_DISHES:
                    fragments.put(id, new DishesFragment());
                    break;
                case MAN_GOODS:
                    fragments.put(id, new GoodsFragment());
                    break;
                case INSIGHT:
                    fragments.put(id, new InsightFragment());
                    break;
                case CREATE_DISH:
                    fragments.put(id, new CreateDishFragment());
                    break;
                case CREATE_GOODS:
                    fragments.put(id, new CreateGoodsFragment());
                    break;
            }
        return fragments.get(id);
    }

    public static void changeFragment(MENU_LIST id) {
        ft = fm.beginTransaction();
        ft.replace(R.id.frame, prepareFragments(id));
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) fm.popBackStack();
        ft.commit();
    }

    public static void changeFragment(AppCompatActivity activity, MENU_LIST id) {
        changeTitle(activity, id);
        changeFragment(id);
    }

    public static void changeTitle(AppCompatActivity activity, MENU_LIST id) {
        switch (id) {
            case SALE:
                activity.getSupportActionBar().setTitle(R.string.title_statistics);
                break;
            case MAN_DISHES:
                activity.getSupportActionBar().setTitle(R.string.title_dishes);
                break;
            case MAN_GOODS:
                activity.getSupportActionBar().setTitle(R.string.title_goods);
                break;
            case INSIGHT:
                activity.getSupportActionBar().setTitle(R.string.title_insight);
                break;
            case CREATE_DISH:
                activity.getSupportActionBar().setTitle(R.string.title_create_dish);
                break;
            case CREATE_GOODS:
                activity.getSupportActionBar().setTitle(R.string.title_create_dish);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getBooleanExtra("appExit", false)) finish();


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //TODO Fill user info
        View headerView = navigationView.getHeaderView(0);
        userEmail = headerView.findViewById(R.id.email);
        displayName = headerView.findViewById(R.id.displayName);
        setupUserInfo(userEmail, displayName);

        //TODO Debug test fragment
        fm = getFragmentManager();

        fragments = new HashMap<>();
        menuInit();

//        try {
//            StorageClient storageClient = StorageClient.getInstance();
//            String ref = "image/ic_menu_camera_new";
//            Uri uri = Uri.parse("android.resource://com.zktechproduction.restaurant_management/drawable/ic_menu_camera");
//            InputStream stream = getContentResolver().openInputStream(uri);
//            storageClient.upload(ref, stream, new OnDownloadUriGenerateListener() {
//                @Override
//                public void onGenerated(Uri uri) {
//                    Toast.makeText(MainActivity.this, "Download path: " + uri.toString(), Toast.LENGTH_LONG).show();
//                }
//            });
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }


    }

    private void setupUserInfo(TextView email, TextView displayName) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email.setText(user.getEmail());
        displayName.setText("Manager");
    }

    private void changeTitle(MENU_LIST id) {
        changeTitle(this, id);
    }

    private void menuInit() {
        changeFragment(MENU_LIST.SALE);
        changeTitle(MENU_LIST.SALE);
    }

    private void signOut() {
        Dialog d = ConfirmationDialog.confirm(this, "Do you want to sign out?", new ConfirmationHandler() {
            @Override
            public void positive() {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

            @Override
            public void negative() {

            }
        });
        d.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Dialog d = ConfirmationDialog.confirm(this, "Do you want to exit?", new ConfirmationHandler() {
                @Override
                public void positive() {
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("appExit", true);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void negative() {

                }
            });
            d.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_statistics) {
            changeFragment(MENU_LIST.SALE);
            changeTitle(MENU_LIST.SALE);
        } else if (id == R.id.nav_manage_dishes) {
            changeFragment(MENU_LIST.MAN_DISHES);
            changeTitle(MENU_LIST.MAN_DISHES);
        } else if (id == R.id.nav_manage_goods) {
            changeFragment(MENU_LIST.MAN_GOODS);
            changeTitle(MENU_LIST.MAN_GOODS);
        } else if (id == R.id.nav_insight) {
            changeFragment(MENU_LIST.INSIGHT);
            changeTitle(MENU_LIST.INSIGHT);
        } else if (id == R.id.signOut) {
            signOut();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
